"""
requeue.py - An SQS manipulation tool

Usage:
    requeue.py move <source> <target>
"""
import random
from typing import Optional

import boto3
from docopt import docopt
import time

class SQS:

    def __init__(self, queue: str):
        self.queue = queue
        self.client = boto3.client('sqs')
        self.batch = []
        self.batch_size = 10

    def read_messages(self, wait_time: int = 10) -> Optional[dict]:
        response = self.client.receive_message(
            QueueUrl=self.queue,
            WaitTimeSeconds=wait_time
        )

        return response['Messages'] if 'Messages' in response else None

    def send_message(self, message: dict, attributes: Optional[dict] = None, delay: int = 0):

        payload = {
            'Id': str(len(self.batch)),
            'MessageBody': message,
            'DelaySeconds': random.randint(0, delay)
        }

        if attributes is not None:
            payload['MessageAttributes'] = attributes

        self.batch.append(payload)

        if len(self.batch) == self.batch_size:

            # Check the in-flight queue size. If we're more than 10 messages in flight, then let's wait.

            while True:
                # Get the queue attribute
                response = self.client.get_queue_attributes(QueueUrl=self.queue,
                                                            AttributeNames=['ApproximateNumberOfMessagesNotVisible'])
                queue_length = int(response['Attributes']['ApproximateNumberOfMessagesNotVisible'])

                # Check if we're more than self.batch_size. If not, then break.
                if queue_length <= self.batch_size:
                    break

                # Sleep for a second.
                print(f'Queue has {queue_length} in-flight messages. Waiting for 1 second.')
                time.sleep(1)

            # Send it

            response = self.client.send_message_batch(
                QueueUrl=self.queue,
                Entries=self.batch
            )
            self.batch = []
            return response

        return None

    def delete_message(self, receipt_handle: str) -> dict:
        return self.client.delete_message(
            QueueUrl=self.queue,
            ReceiptHandle=receipt_handle
        )


def move_messages(source: str, target: str, max_delay: int = 0):
    source_queue = SQS(source)
    target_queue = SQS(target)

    delete_list = []

    while True:
        messages = source_queue.read_messages()
        if messages is None or len(messages) == 0:
            break

        for message in messages:
            print(message)
            response = target_queue.send_message(message=message['Body'])
            delete_list.append(message['ReceiptHandle'])
            if response is not None:
                for handle in delete_list:
                    source_queue.delete_message(handle)


def main(args):
    if args['move']:
        move_messages(source=args['<source>'], target=args['<target>'],
                      max_delay=900)


if __name__ == '__main__':
    main(docopt(__doc__))
