# Re-queue
Move SQS messages from one queue to another. This is particularly useful for re-queueing messages from a dead letter queue.

## Getting started

Install your dependencies using pipenv

```
pipenv install
```

Use the tool.

```
requeue.py move <source> <target>
```

Please note that you may need to set your `AWS_PROFILE` for this tool to use the appropriate credentials.